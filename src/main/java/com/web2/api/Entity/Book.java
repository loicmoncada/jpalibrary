package com.web2.api.Entity;

import java.util.Set;

import com.fasterxml.jackson.annotation.JsonIgnore;

// import org.hibernate.annotations.ManyToAny;
// import org.springframework.boot.autoconfigure.amqp.RabbitConnectionDetails.Address;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.JoinTable;
import jakarta.persistence.ManyToMany;
// import jakarta.persistence.ManyToMany;
import jakarta.persistence.ManyToOne;
// import jakarta.persistence.OneToMany;
// import jakarta.persistence.OneToOne;
// import jakarta.persistence.Lob;
import jakarta.persistence.Table;

@Entity
@Table(name = "Book")
public class Book {

  @GeneratedValue(strategy = GenerationType.AUTO)
  @Id
  private Integer id;

  @Column(name = "title", length = 100, nullable = false)
  private String title = "none";

  @Column(name = "description", length = 4095, nullable = true,columnDefinition = "TEXT")
  private String description = "";

  @Column(name = "available")
  private Boolean available = true;

  @JsonIgnore
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "category")
  private Category category;
  @ManyToMany
    @JoinTable(
        name = "author_book",
        joinColumns = @JoinColumn(name = "book_id"),
        inverseJoinColumns = @JoinColumn(name = "author_id")
    )
    private Set<Author> authors;

  public Book() {
  }

  public Book(String title, String description, Boolean available) {
    this.title = title;
    this.description = description;
    this.available = available;
  }

  public Integer getId() {
    return this.id;
  }

  public String getTitle() {
    return this.title;
  }

  public String getDescription() {
    return this.description;
  }

  public Boolean getAvailable() {
    return this.available;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public Category getCategory() {
    return category;
  }

  public void setCategory(Category category) {
    this.category = category;
  }

  public void setAvailable(Boolean available) {
    this.available = available;
  }

  public Set<Author> getAuthors() {
    return authors;
  }

  public void setAuthors(Set<Author> authors) {
    this.authors = authors;
  }

}