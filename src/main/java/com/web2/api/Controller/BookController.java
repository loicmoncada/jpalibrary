package com.web2.api.Controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.web2.api.Entity.Book;
import com.web2.api.Repository.BookRepository;

@CrossOrigin(origins = "http://localhost:9005")
@RequestMapping("/api")
@RestController
public class BookController {
	@Autowired
	private BookRepository bookRepository;

	public BookController(BookRepository bookRepository) {
		this.bookRepository = bookRepository;
	}

	@GetMapping("/books")
	public Iterable<Book> findAllBook() {
		return this.bookRepository.findAll();
	}

	@GetMapping("/books/{id}")
	public ResponseEntity<Book> getBookById(@PathVariable("id") Long id) {
		Optional<Book> bookData = bookRepository.findById(id);

		if (bookData.isPresent()) {
			return new ResponseEntity<>(bookData.get(), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@GetMapping("/books/title/{title}")
	public ResponseEntity<Book> getBookByTtile(@PathVariable String title) {
		Book bookData = bookRepository.findByTitle(title);

		try {
			return new ResponseEntity<Book>(bookData, HttpStatus.OK);
		} catch(Exception e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@PutMapping("/books/{id}")
	public ResponseEntity<Book> updateBook(@PathVariable("id") Long id, @RequestBody Book book) {
		Optional<Book> BookData = bookRepository.findById(id);

		if (BookData.isPresent()) {
			Book _Book = BookData.get();
			_Book.setTitle(book.getTitle());
			_Book.setDescription(book.getDescription());
			_Book.setAvailable(book.getAvailable());
			return new ResponseEntity<>(bookRepository.save(_Book), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@DeleteMapping("/books/{id}")
	public ResponseEntity<HttpStatus> deleteBooks(@PathVariable("id") Long id) {
		try {
			bookRepository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@PostMapping("/books")
	public Book addOneBook(@RequestBody Book book) {
		return this.bookRepository.save(book);
	}
}