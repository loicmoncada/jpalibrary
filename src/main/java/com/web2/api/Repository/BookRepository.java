package com.web2.api.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.web2.api.Entity.Book;


@Repository
public interface BookRepository extends JpaRepository<Book, Long> {
    Book findByTitle(String title);
}