package com.web2.api.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.web2.api.Entity.Author;
import com.web2.api.Repository.AuthorRepository;

@CrossOrigin(origins = "http://localhost:9005")
@RequestMapping("/api")
@RestController
public class AuthorController {
    @Autowired
	private AuthorRepository authorRepository;

	public AuthorController(AuthorRepository authorRepository) {
		this.authorRepository = authorRepository;
	}
    @GetMapping("/author")
	public Iterable<Author> findAllAuthor() {
		return this.authorRepository.findAll();
	}
    @PostMapping("/author")
	public Author addOneAuthor(@RequestBody Author author) {
		return this.authorRepository.save(author);
	}
}
