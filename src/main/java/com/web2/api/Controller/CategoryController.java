package com.web2.api.Controller;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.web2.api.Entity.Category;
import com.web2.api.Repository.CategoryRepository;

@CrossOrigin(origins = "http://localhost:9005")
@RequestMapping("/api")
@RestController
public class CategoryController {

    @Autowired
    private CategoryRepository categoryRepository;

    public CategoryController(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    @GetMapping("/category")
    public Iterable<Category> findAllCategory() {
        return this.categoryRepository.findAll();
    }

    @PostMapping("/category")
    public Category addOneCategory(@RequestBody Category category) {
        return this.categoryRepository.save(category);
    }

}