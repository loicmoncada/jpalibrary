package com.web2.api.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.web2.api.Entity.Author;

@Repository
public interface AuthorRepository extends JpaRepository<Author,Long> {
    
}
